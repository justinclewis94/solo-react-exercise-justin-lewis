import React from "react";
import { Scrollbars } from 'react-custom-scrollbars-2';


export default class RepList extends React.Component {
		
	render() {
		if(Array.isArray(this.props.repList)) {
			
			const handleClick = this.props.handleClick;
						
			return ( <div className="replist">
			<h4>{this.props.inputState}{" "}
			{this.props.category.charAt(0).toUpperCase() + this.props.category.slice(1)}</h4>
			<Scrollbars className="replist-scrollbox" >
				{ this.props.repList.map(function (rep, key) {
					let party = Array.from(rep.party)[0];
					return (<div className="replist-item row"
							key={key}
							onClick={() => handleClick(key)}>
						<div className="replist-name col-10">{rep.name.replace(/[^\x00-\x7F]/g,'')}</div>
						<div className="replist-party col-2"><p className={party} >{party}</p></div>
					</div>
					)
				})}
			</Scrollbars>
			</div>			)
		} else {
			return <div className="replist-warning">{this.props.repList}</div>;
		}
	}
		
}

