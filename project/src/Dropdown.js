import React from "react";
import Select from 'react-select';

export default class Dropdown extends React.Component {
		
	render() {
		return <div>
				<label>{this.props.label}</label>
				<Select options={this.props.options} 
						onChange={this.props.handleChange}
						defaultValue={this.props.defaultOption}
				/>
		</div>
		;
	}
	
}

