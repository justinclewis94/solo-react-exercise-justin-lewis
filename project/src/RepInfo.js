import React from "react";

export default class RepInfo extends React.Component {
  
  render() {
	if(typeof this.props.repInfo === 'object') {
		return (<div className="repinfo" >
		<h4>{this.props.category.charAt(0).toUpperCase() 
		+ this.props.category.substring(1, this.props.category.length - 1)} 
		{" "}{this.props.repInfo.name.replace(/[^\x00-\x7F]/g,'')}</h4>
		
		<label>Party</label>
		<p>{this.props.repInfo.party}</p>
		
		<label>State / District</label>
		<p>{this.props.repInfo.state} / {this.props.repInfo.district}</p>
		
		<label>Phone</label>
		<p>{this.props.repInfo.phone}</p>
		
		<label>Office</label>
		<p>{this.props.repInfo.office}</p>
		
		<label>Website</label>
		<p><a href={this.props.repInfo.link} target="_blank">{this.props.repInfo.link}</a></p>
		
		</div>);
	}
	else {
		return <div className="repinfo-warning">{this.props.repInfo}</div>;
	}
  }
  
}

