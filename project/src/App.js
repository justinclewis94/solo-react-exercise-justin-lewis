import React from "react";
import Dropdown from './Dropdown';
import RepList from './RepList';
import RepInfo from './RepInfo';
import {usStates} from './usStates.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


export default class App extends React.Component {
	
	state = {
		inputCategory: "representatives",
		inputState: null,
		repList: 'Please select a State to view your representatives',
		repDetails: '',
	};
		
	/*
	* Called on dropdown change
	* Set inputCategory and inputState
	* Then run api call to set repList 
	*/
	handleChange = (selectedOption) => {
		this.setState({[selectedOption.name]: selectedOption.value}, function () {
			
			if(!this.state.inputCategory || !this.state.inputState) {
				this.setState({ repList: 'Please select a state to view your '+this.state.inputCategory });
				return;
			}
			
			// Set API URL based on chosen category 
			let apiURL = 'http://localhost:3000/' + this.state.inputCategory + '/' + this.state.inputState;
			
			// double check
			if(window.location.port === '3000')
			{
				this.setState({ repList:"You're running on port 3000, which is reserved for the API. "
				+ "The API may not be running. "
				+ "I recommend to 'npm start' the api portion first, and then start this project."});
			}
			
			
			// Asyncronous API call
			fetch(apiURL)
				.then((response) => response.json())
				.then((data) => {
					this.setState({ repList: data.results });
					this.setState({ repDetails: "Please select a " 
					+ this.state.inputCategory.substring(0, this.state.inputCategory.length - 1)});
			});
		});
    }
	
	/*
	* Called on representative click
	* Set repDetails based on key of chosen representative
	*/
	handleRepClick = (key) => {
		this.setState({repDetails: this.state.repList[key]});
	}
		
	Category = [
		{ name: "inputCategory", label: "Representatives", value: "representatives"},
		{ name: "inputCategory", label: "Senators", value: "senators"},
	];
			
	render() {
		return (
			<div className="App">
				<div className="container-liquid main-container">
					<div className="card">
						<h5 className="card-header">Who's My Representative?</h5>
						<div className="card-body">
							<div className="row rep-form">
								<div className="col-6">
									<Dropdown label="Category:"
											  options={this.Category}
											  handleChange={this.handleChange}
											  defaultOption = {{name: "inputCategory", label: "Representatives", value: "representatives"}} />
								</div>
								<div className="col-6">
									<Dropdown label="State:"
											  options={usStates}
											  handleChange={this.handleChange} />
								</div>
							</div>
							
							<div className="row">
								<div className="col-6">
									<RepList repList={this.state.repList} 
										handleClick={this.handleRepClick}
										inputState={this.state.inputState}
										category={this.state.inputCategory}/>
								</div>
								<div className="col-6">
									<RepInfo repInfo={this.state.repDetails}
										category={this.state.inputCategory}/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	  );
	}
}


